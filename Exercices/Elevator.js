let Elevator = {
    field: [10, 15],
    position: [1, 1],
    up: () => {
        Elevator.print(--Elevator.position[0] < 1 ? ++Elevator.position[0] : Elevator.position[0], Elevator.position[1]);
    },
    right: () => {
        Elevator.print(Elevator.position[0], ++Elevator.position[1] > Elevator.field[1] ? --Elevator.position[1] : Elevator.position[1]);
    },
    down: () => {
        Elevator.print(++Elevator.position[0] > Elevator.field[0] ? --Elevator.position[0] : Elevator.position[0], Elevator.position[1]);
    },
    left: () => {
        Elevator.print(Elevator.position[0], --Elevator.position[1] < 1 ? ++Elevator.position[1] : Elevator.position[1]);
    },
    setPosition: (posX = 1, posY = 1) => {
        Elevator.print(Elevator.position[0] = posX, Elevator.position[1] = posY);
    },
    print: (posX = Elevator.position[0], posY = Elevator.position[1]) => {
        let elevator = '';
        if (posX > 0 && posY > 0 && posX < Elevator.field[0] + 1 && posY < Elevator.field[1] + 1)
            for (let i = 0; i < Elevator.field[0]; i++) {
                for (let j = 0; j < Elevator.field[1]; j++)
                    if (i == posX - 1 && j == posY - 1)
                        elevator +=  '| X |';
                    else elevator += '|   |';
                elevator += '\n';          
            }
        console.log(elevator == '' ? 'Selecione uma posição dentro de ' + Elevator.field[0] + 'X' + Elevator.field[1] : elevator);         
    }    
}

Elevator.print();
