function BMI(mass, height) {
    let BMI = mass / height ** 2;
    console.log('Seu IMC: ' + BMI);

    if (BMI < 17)
        console.log('Muito abaixo do peso');
    else if (BMI < 18.5)
        console.log('Abaixo do peso');
    else if (BMI < 25)
        console.log('Peso normal');
    else if (BMI < 30)
        console.log('Acima do peso');
    else if (BMI < 35)
        console.log('Obesidade I');
    else if (BMI < 40)
        console.log('Obesidade II (Severa)');
    else
        console.log('Obesidade III (Mórbida)');
}
