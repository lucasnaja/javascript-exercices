function weekDay(year, month, day) {
    let myDate = new Date(year + '/' + month + '/' + day),
        dayWeeks = ['Domingo', 'Segunda-feira', 'Terça-feira',
                    'Quarta-feira', 'Quinta-feira', 'Sexta-feira',
                    'Sábado'],
        monthYear = ['Janeiro', 'Fevereiro', 'Março',
                     'Abril', 'Maio', 'Junho', 'Julho',
                     'Agosto', 'Setembro', 'Outubro',
                     'Novembro', 'Dezembro'];

    console.log('Dia da semana: ' + dayWeeks[myDate.getDay()] +
                '\nMês: ' + monthYear[myDate.getMonth()]);
}
